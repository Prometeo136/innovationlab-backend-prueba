.PHONY: help build up requirements clean lint test help
PROJECT=CREDITOS

ROOT_DIR=$(shell pwd)
SOURCE_DIR=$(ROOT_DIR)/
DOCKER_COMPOSE_FILE=local
POST_MESSAGE=
pip_install= pip install -r

help:
	@echo 'Makefile for: ${PROJECT}'
	@echo ''
	@echo '  Options:'
	@echo '    setup             set up the project'
	@echo '    start             start the project'
	@echo '    stop              stop the container(s)'
	@echo '    restart           restart the container(s)'
	@echo '    tests             run tests'
	@echo '    logs              show logs'
	@echo '    migrations        make migrations and migrate'
	@echo ''

setup:
	@echo "Installing precommit hooks ${POST_MESSAGE}"
	#pre-commit install
	@echo "Building image from docker compose ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml build --no-cache

start:
	@echo "Starting the container ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml up -d

stop:
	@echo "Stopping the container ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml stop

restart:
	@make -s stop
	@make -s start

tests:
	@echo "Running tests ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml exec django pytest

logs:
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml logs -f

migrations:
	@echo "Making migrations ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml exec django python manage.py makemigrations
	@echo "Running migrations ${POST_MESSAGE}"
	docker-compose -f $(DOCKER_COMPOSE_FILE).yaml exec django python manage.py migrate
