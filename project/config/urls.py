# -*- coding: utf-8 -*-
from rest_framework.authtoken.views import obtain_auth_token
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    # Django Admin, use {% url 'admin:index' %}
    # User management
    path("users/", include("apps.users.urls", namespace="users")),
    path("creditos/", include("apps.creditos.api.urls")),
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# API URLS
urlpatterns += [
    # API base url
    path("api/", include("config.api_router")),
    # DRF auth token
    path("auth-token/", obtain_auth_token),
]
