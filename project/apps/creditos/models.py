from django.db import models
from django.core.exceptions import ValidationError


def validate_negative(val):
    if val < 0:
        raise ValidationError(
            "%(value) is not a positive number", params={'value': val})


class Client(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    amount = models.FloatField(
        blank=False, null=False, validators=[validate_negative], default=0.0)
    debt = models.FloatField(blank=False, null=False,
                             validators=[validate_negative])
    score_sbs = models.CharField(max_length=20, default="")
    score_ia = models.SmallIntegerField(
        default=0, validators=[validate_negative])
    status = models.BooleanField(null=True)

    def __str__(self):
        return self.first_name
