from rest_framework.serializers import ModelSerializer
from apps.creditos.models import Client


class CreditsSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"
