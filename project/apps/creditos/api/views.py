import random
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from .serializers import CreditsSerializer
from apps.creditos.models import Client
from django.shortcuts import get_object_or_404


class ClientRequest(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        data = {
            "first_name": request.data.get('first_name'),
            "last_name": request.data.get('last_name'),
            "amount": request.data.get("amount")
        }

        # Getting debt from SBS api .......
        debt = random.uniform(0, 500)
        data['debt'] = debt

        # Getting score from IA
        score_ia = random.randint(0, 10)
        data['score_ia'] = score_ia

        # Getting score from sentinel ...
        choices = ["bueno", "regular", "malo"]
        sentinel_score = random.choice(choices)
        data['score_sbs'] = sentinel_score

        serialized_request = CreditsSerializer(data=data)
        if serialized_request.is_valid():
            serialized_request.save()
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response(serialized_request.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        clients = Client.objects.all()
        serialized_clients = CreditsSerializer(clients, many=True)
        return Response({"clients": serialized_clients.data})


class ApproveRejectRequest(APIView):
    permission_classes = [AllowAny]

    def post(self, request, client_id):
        # user_instance = get_object_or_404(User, id=id)
        client = get_object_or_404(Client, id=client_id)
        data = {
            "status": request.data.get("status")
        }

        serialized_request = CreditsSerializer(client, data=data, partial=True)
        if serialized_request.is_valid():
            serialized_request.save()
            return Response(serialized_request.data, status=status.HTTP_200_OK)
        else:
            return Response(serialized_request.errors, status=status.HTTP_400_BAD_REQUEST)
