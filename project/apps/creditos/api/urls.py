from django.urls import path
from . import views

urlpatterns = [
    path('client-request/', views.ClientRequest.as_view(), name='client_request'),
    path('approve-reject-request/<int:client_id>/',
         views.ApproveRejectRequest.as_view(), name='approve_request'),
]
