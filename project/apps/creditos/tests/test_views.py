import pytest
from rest_framework.test import APIClient
from django.urls import reverse
from apps.creditos.models import Client
from django.test import TestCase
from mixer.backend.django import mixer

pytestmark = pytest.mark.django_db


class TestRequestView(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.client_1 = mixer.blend(Client, first_name="Andrés",
                                    last_name='Vargas', amount=1500.00)
        self.client_2 = mixer.blend(Client, first_name="Fernanda",
                                    last_name='Mendoza', amount=1800.50)

    def test_list_clients(self):
        url = reverse("client_request")
        response = self.client.get(url)
        assert response.status_code == 200
        assert response.data['clients'] != []

    def test_create_request_success(self):
        data = {
            "first_name": "Vicky",
            "last_name": "Medina",
            "amount": 8000.25
        }
        url = reverse("client_request")
        response = self.client.post(url, data=data)
        print(response.data)
        assert response.status_code == 200

    def test_create_request_fail(self):
        data = {
            "first_name": "Vicky",
            "amount": 8000.25
        }
        url = reverse("client_request")
        response = self.client.post(url, data=data)
        assert response.status_code == 400
